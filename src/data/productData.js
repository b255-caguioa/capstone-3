const productData = [

	{
		id: "01",
		name: "Vans Authentic DX Blocked",
		description: "Black/white",
		price: 3000,
		onOffer: true
	},
	{
		id: "02",
		name: "Vans Slip-on",
		description: "Yellow/white",
		price: 1799,
		onOffer: true
	},
	{
		id: "03",
		name: "Vans Authentic DX Blocked",
		description: "Red/white",
		price: 3000,
		onOffer: true
	}

]

export default productData;