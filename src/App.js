// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Products from './pages/Products';
import ProductsView from './pages/ProductsView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import UserCart from './pages/UserCart';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
});

  const unsetUser = () => {
    localStorage.clear();
  }


  return (
    
    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element = {<Home/>} />
                    <Route path="/products" element = {<Products/>} />
                    <Route path="/products/:productId" element = {<ProductsView/>} />
                    <Route path="/register"element = {<Register/>}/>
                    <Route path="/login" element = {<Login/>}/>
                    <Route path="/logout" element = {<Logout/>}/>
                    <Route path="/admindashboard" element = {<AdminDashboard/>}/>
                    <Route path="/createproduct" element = {<CreateProduct/>}/>
                    <Route path="/updateproduct/:productId" element = {<UpdateProduct/>}/>
                    <Route path="/cart" element = {<UserCart/>}/>
                    <Route path="/*" element = {<Error/>}/>
                </Routes>
            </Container>
        </Router>
    </UserProvider>

  );
}

export default App;