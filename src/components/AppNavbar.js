import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';
import './AppNavBar.css';

export default function AppNavbar(){

  const { user } = useContext(UserContext);
  console.log(user)
  console.log(user.isAdmin)

  return(

    <Navbar className="navbar-dark" bg="dark" expand="lg">
          <Container>
            <Navbar.Brand as={Link} to="/">FeetLab</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
                {(user.isAdmin === false) ?
                  <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
                  :
                  <React.Fragment>
                  </React.Fragment>
                }
                {(user.id !== null) ?
                  <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  :
                  <React.Fragment>
                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                  </React.Fragment>
                }

                {(user.isAdmin === true) ?
                  <Nav.Link as={Link} to="/admindashboard">Admin</Nav.Link>
                  :
                  <React.Fragment>
                  </React.Fragment>
                }
                

              </Nav>
            </Navbar.Collapse>

        </Container>
    </Navbar>
  )
}
