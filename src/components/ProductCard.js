import { useState, useEffect} from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard(productProp) {

	console.log(productProp)

	const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(30);

	const { name, description, price, _id } = productProp.productProp;

	function enroll() {
			if(count < 30) {
				setCount(count + 1)
				// console.log('Enrollee: ' + count)
				setSeatCount(seatCount - 1)
				// console.log('Seats: ' + seatCount)
			} //else {
				//alert("No more seats available.")
			//}
	}

	useEffect(() => {
		if(seatCount === 0){
			alert("Out of stock.")
		}
	}, [seatCount])



	console.log(productProp.productProp)

	return (
	<Container fluid>
		<Row md={10}>
			<Col className="d-flex">
				<Card className="flex-fill" key={productProp}>
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>
				        <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>
	)

}