import { Row, Col, Card, Button } from 'react-bootstrap';
import img1 from './images/img-1.jpg'
import img2 from './images/img-2.jpg'
import img3 from './images/img-3.jpg'

export default function Highlights(){
	return(

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card>
					<Card.Img variant="top" src={img1} />
				  	<Card.Body>
				    	<Card.Title>Vans Authentic DX Blocked</Card.Title>
				    	<Card.Text>
				    		Black/White<br/>
					      	PHP3,000
				    	</Card.Text>
				    <Button variant="primary">Buy now</Button>
				  	</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card>
					<Card.Img variant="top" src={img2} />
				  	<Card.Body>
				    	<Card.Title>Vans Slip-On</Card.Title>
				    	<Card.Text>
				    		Yellow/White<br/>
					    	PHP1,799
				    	</Card.Text>
				    <Button variant="primary">Buy now</Button>
				  	</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card>
					<Card.Img variant="top" src={img3} />
				  	<Card.Body>
				    	<Card.Title>Vans Authentic DX Blocked</Card.Title>
				    	<Card.Text>
					    	Red/White<br/>
					    	PHP3,000
				    	</Card.Text>
				    <Button variant="primary">Buy now</Button>
				  	</Card.Body>
				</Card>
			</Col>

		</Row>
	)
}