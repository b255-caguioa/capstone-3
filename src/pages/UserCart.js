import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import productData from '../data/productData';
import ProductCard from '../components/ProductCard';


export default function UserCart() {

	const [cart, setCart] = useState([]);


	useEffect(() => {
		fetch('http://localhost:4000/users/cart', {
				headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCart(data) 
		})
	}, [])

	console.log(cart)
	





	return (
		<>
			<h1>My Cart</h1>
			<>
				{cart.map(users => {
					return (
						<ProductCard key={users.orderedProduct} productProp={users} />
					)
				})}
			</>
		</>
	)
}