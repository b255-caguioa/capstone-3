import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether the submit button is enabled or disabled
	// Conditional rendering
	const [isActive, setIsActive] = useState(false);


	function registerUser (e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {

				// Clear input fields
				setEmail("");
				setPassword1("");
				setPassword2("");

				Swal.fire({
					title: "Registration Successful",
					icon: "success",
					text: "Welcome to FeetLab!"
				})

				navigate("/login");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}

		})
	}


	useEffect(() => {
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2])


	return (
	  	(user.id) ? 
	  		<Navigate to="/products" /> : (
	    		<Form onSubmit={(e) => registerUser(e)}>

				  	<h1 className="text-center my-3">Registration</h1>

			      	<Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email"
				        	value={email}
				        	onChange={(e) => {setEmail(e.target.value)}}
				        	placeholder="Enter email" />
				        <Form.Text className="text-muted">
				          We'll never share your email with anyone else.
				        </Form.Text>
			     	</Form.Group>

			      	<Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value={password1}
				        	onChange={(e) => {setPassword1(e.target.value)}}
				        	placeholder="Enter Your Password" />
				    </Form.Group>

			      	<Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Verify Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value={password2}
				        	onChange={(e) => {setPassword2(e.target.value)}}
				        	placeholder="Verify Your Password" />
			     	</Form.Group>
				    {isActive ? (
				        <Button variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
				       	) : (
				        <Button variant="primary" disabled id="submitBtn">
				        	Submit
				        </Button>
				    )}
			     
			    </Form>	
		)

	)
}
