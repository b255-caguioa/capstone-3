import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function Login(props) {

	// Allows us to consume the User context object and it's properties to be used for validation.
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);


	const retrieveUserDetails = (token) => {
	  fetch('http://localhost:4000/users/details', {
	    headers: {
	      'Authorization': `Bearer ${token}`
	    }
	  })
	    .then(res => res.json())
	    .then(data => {
	      console.log(data);

	      // Global user state for validation across the whole app
	      setUser({
	        id: data._id,
	        isAdmin: data.isAdmin
	      });
	    });
	};


	// Function to simulate user registration
	function authenticate(e) {		
		e.preventDefault()

		fetch('http://localhost:4000/users/login', {
	        method: 'POST',
	        headers: {
	            'Content-Type': 'application/json',
	            'Accept': 'application/json'
	        },
	        body: JSON.stringify({
	            email: email,
	            password: password
	        })
	    })
	    .then(res => res.json())
	    .then(data => {
	        console.log(data);

	        if(data?.access){
	            localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

	            Swal.fire({
	                title: "Login Successful",
	                icon: "success",
	                text: "Welcome to FeetLab!"
	            });
	        } else {
	            Swal.fire({
	                title: "Authentication Failed",
	                icon: "error",
	                text: "Please check your login details and try again."
	            });
	        };
	    })
	    .catch(error => {
	        console.error('Error:', error);
	        Swal.fire({
	          title: "Authentication Failed",
	          icon: "error",
	          text: "An error occurred during authentication. Please try again."
	        });
	    });

		// Clear the input fields and states
		setEmail("");
		setPassword("");
	}



	useEffect(() => {
		if (email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password, setIsActive]);

	console.log(user.id)

	return (
		(user.id !== null && user.id !== undefined) ?
		<Navigate to="/products" />
		:
		<Row className="mt-3 mb-3 d-flex justify-content-center">
			<Col xs={12} md={4}>
				<Form onSubmit={e => authenticate(e)}>

					<h1 className="text-center my-3">Login</h1>
					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Label>Email address</Form.Label>
						<Form.Control 
							type="email" 
							placeholder="Enter email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="userPassword">
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password" 
							placeholder="Password"
							value={password}
							onChange={e => setPassword(e.target.value)}
							required
						/>
					</Form.Group>
				
					{
					isActive ?
					
						<Button className="text-center" variant="primary" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button className="text-center"  variant="primary" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
					}
				</Form>
			</Col>
		</Row>	
	);
}