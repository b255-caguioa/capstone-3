import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import productData from '../data/productData';

export default function Products() {

	const [products, setProducts] = useState([]);

	// Checks to see if the mock data was captured.
	// console.log(coursesData);
	// console.log(coursesData[0]);


	useEffect(() => {
		fetch('http://localhost:4000/products/', {
				headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data) 
		})
	}, [])

	console.log(products)

	return (
		<>
			<h1>Products</h1>
			<>
				{products.map(product => {
					return (
						<ProductCard key={product._id} productProp={product} />
					)
				})}
			</>
		</>
	)
}