import { useState, useEffect} from 'react';
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import productData from '../data/productData';
// import Sidebar from '../components/Sidebar';

export default function AdminDashboard() {
	const [products, setProducts] = useState([]);



	useEffect(() => {
		fetch('http://localhost:4000/products/all', {
				headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
		}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProducts(data) 
			// console.log(products)
		})
	}, [])

	const archiveProduct = (productId) => {
			fetch(`http://localhost:4000/products/archive/${productId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					isActive: false
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data === false){
					Swal.fire({
						title: "Unable to activate",
						icon: "error"
					})
				} else {
					Swal.fire({
						title: "Product successfully archived",
						icon: "success"
					})
				}

				
			})
	}

	const activateProduct = (productId) => {
		fetch(`http://localhost:4000/products/activate/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === false){
				Swal.fire({
					title: "Unable to activate",
					icon: "error"
				})
			} else {
				Swal.fire({
					title: "Product successfully activated",
					icon: "success"
				})
			}

			
		})
	}


	return (

		<>
		
			<div className = "text-center">
			<br/>
				<h1>Admin Dashboard</h1>
				<Row className="mt-3 mb-3">
					<Col xs={2} md={2}>
						<Link as={Link} to="/createproduct">
							<Button className="bg-primary">Add New Product</Button>
						</Link>
					</Col>
					<Col xs={2} md={2}>
						<Link as={Link} to="/createproduct">
							<Button className="bg-success">Show Orders</Button>
						</Link>
					</Col>
				</Row>
			
			</div>
			<br/>
	
			<table className="table table-striped">
			<thead className="thead-dark">
			    <tr>
			      <th scope="col">Name</th>
			      <th scope="col" colSpan="3">Description</th>
			      <th scope="col">Price</th>
			      <th scope="col">Availability</th>
			      <th scope="col">Actions</th>
			    </tr>
			  </thead>
			  <tbody>
			  	{products.map((data) => {
			  		console.log(data._id)
			  		return(
			  			<tr>
			  			  <th scope="row">{data.name}</th>
			  			  <td colSpan="3">{data.description}</td>
			  			  <td>{data.price}</td>
			  			  <td>{data.isActive ? "Available" : "Not Available"}</td>
			  			  <td>
			  			  	<Link as={Link} to={`/updateproduct/${data._id}`}>
			  			  		<Button className="bg-success">Update</Button>
			  			  	</Link>
			  			  	<br/>
			  			  	<br/>
			  			  	{
			  			  		(data.isActive === true) ?
			  			  		<Button onClick={(e) => {archiveProduct(data._id)}} className="bg-secondary">Disable</Button>
			  			  		:
			  			  		<Button onClick={(e) => {activateProduct(data._id)}} className="bg-primary">Enable</Button>
			  			  	}
			  			  </td>
			  			</tr>			  		
			  		)
			  	})}
			  </tbody>
			</table>
		</>
	)
}