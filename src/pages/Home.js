import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import ProductCard from '../components/ProductCard';

export default function Home() {

	const data = {
		title: "FeetLab",
		content: "The number 1 online shoe store in the region!"
	}

	return (
		<>
			<Banner data={data}/>
			<Highlights/>
		</>
	)
}